﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_UpdateScore : MonoBehaviour
{

    public Text timeText;
    private int playerScore;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        playerScore = GameObject.Find("GameProcesses").GetComponent<Score>().playerScore;
        timeText.text = playerScore.ToString();
    }
}
