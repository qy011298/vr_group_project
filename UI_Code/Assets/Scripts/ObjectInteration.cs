﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInteration : MonoBehaviour
{
    /*
    NOTE:   This must be attached to the FirstPersonCharacter Gameobject,
            NOT the FPSController. 

            All objects that are interactable need to be on layer 8

            The interaction UI method, is expected to be under the Text GameObject called
            'objText;, which is the actual Text, not the Canvas.
    */
    public float objectInteractionDistance = 5;
    // This refers to the GameObject that is the Text Box for "press e to interact".
    private GameObject UI_objMsg = null;

    private GameObject GameProcesses = null;

    // Start is called before the first frame update
    void Start()
    {
        UI_objMsg = GameObject.Find("objText");
        GameProcesses = GameObject.Find("GameProcesses");
    }

    // Update is called once per frame
    void Update()
    {
        // Only objects on layer 8 are interactable - any others aren't.
        // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 1 << 8;

        RaycastHit hit;
        // If ray intersects an object we want to interact with
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, objectInteractionDistance, layerMask))
        {
            // Show interaction message.
            UI_objMsg.SetActive(true);
            Debug.DrawLine(transform.position, hit.point, Color.green);

            // If we are looking at an object, and we press 'e', remove object
            if (Input.GetKeyDown("e"))
            {
                hit.collider.gameObject.SetActive(false);
                GameProcesses.GetComponent<Score>().playerScore++;
                UI_objMsg.SetActive(false);
            }
        }
        else
        {
            // Stop showing interaction method.
            UI_objMsg.SetActive(false);
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * objectInteractionDistance, Color.red);
        }
    }
}
