# Important Stuff from Specification

## Assignment Description

This task is to be undertaken in groups of typically 4 or 5 people, but although you are working as a group, it is important that each member has a specific task to do – which is to be documented individually.

This is the era of ‘fake news’, with liars propagating ‘alternative facts’. The object of this assignment is to develop an immersive virtual environment, with suitable interaction and activity, which is largely a valid representation of a scenario, but which has various fake aspects. It could be a historical setting, a satire, a scientific theory, etc. You may decide to turn it into a ‘game’ whose object is to pick out what is true and what is fake, perhaps including true but implausible items – a VR equivalent of the “Unbelievable Truth” on Radio 4.

You are free to choose any topic, but it must follow this context.

## Project Ins and Outs

You should first meet with your group and decide what you are going to build. It doesn’t matter what your simulation is intended to do. The key point is the simulation should be designed with a definite purpose in mind.

You will be expected to be able to demonstrate your knowledge of the core Virtual Reality concepts which are covered in the course, in particular, ‘human’ concepts such as: realism, believability and usability, and technical concepts such as: lighting, physics/animation and geometry. Remember, the purpose is not to create a photorealistic animation, but to use the tools available to create an interactive, believable simulation.

You should try to consider the following design goals in both your group project and your individual submission: Interactivity (Intuitive, Simple), User Experience (Immersion & Presence), Design (Realism Vs Believability), Purpose (Application & Functionality).

Once you have a plan, assign different responsibilities to each member and agree a timeline. You will need to work closely to ensure the different aspects work properly together. Note documentation and research are part of the project.

The overall project will typically be achieved using a variety of products, coordinated with Unity. So, for instance, detailed models of the environment or characters imported into it could be built with SketchUp, Blender, Maya or SolidWorks, or similar. Divide the tasks accordingly to each person’s expertise.

## Group Report Requirements:

This should contain the following
 - Title of World
 - Aims of World
 - Screen shot(s)
 - Instructions on how to use it.
 - Relevant background research, with suitable references
 - Commentary on interactivity, user experience, design and purpose
 - Both when incorporated in the design and when reflecting the result

Brief statement of tasks done by each member of the group.

## Individual Report Requirements:

The Individual submission should describe your individual contribution to the project, saying what you have done and why, and how you have attempted to incorporate different virtual reality concepts. You can refer to the Group document.

Usually this is a brief report (up to 4 pages of main text), but a video demonstrating your work, or some suitable audio, is also acceptable.

Such video or audio could be included in the report or could be a submission on its own, with some relevant information.

For instance, if your task is to do scripting, then a video showing how the scripts operate perhaps with fully commented code may be appropriate.
If you are doing the audio, then some examples could be included.
A fact sheet listing sources, references, etc. could go with such a submission.

