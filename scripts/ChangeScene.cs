using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public int score = 0;
    public int sceneNo = 0;

    public void NextScene(string sceneName)
    {
	// some check to see if we are at final level will need to be added here
        sceneNo += 1;
	// this works with naming conventions Level1, Level2 and assumes that we start on a menu by default.
        SceneManager.LoadScene("Level" + sceneNo.ToString());
    }

}
