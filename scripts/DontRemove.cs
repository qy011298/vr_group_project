using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DontRemove : MonoBehaviour
{
    void Awake()
    {
	// The GameMaster object needs to be given the tag "master"
        // get objs that are part of master tag
        GameObject[] objs = GameObject.FindGameObjectsWithTag("master");

        // this removes if there are accidentally multiple instances of master
        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        // this sets the game object to not be removed on scene change
        DontDestroyOnLoad(this.gameObject);
    }
}